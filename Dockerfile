# container
FROM node:14.15.1

# inspired by https://github.com/renyufu/electron-packager

RUN dpkg --add-architecture i386
RUN apt-get update
RUN apt-get install wine wine32 wine64 zip -y

RUN npm install electron-packager -g

CMD [""]